import React, { useState, useEffect } from 'react'
import moment from 'moment'

const start_time = 1579386588;

const restTime = (start_time, current_time) => {
    const dd = moment.unix(current_time).diff(start_time, 'days')
    const hr = moment.unix(current_time).diff(start_time, 'h') % (60 * 60 * 24)
    const min = moment.unix(current_time).diff(start_time, 'm') % (60 * 60)
    const sec = moment.unix(current_time).diff(start_time, 's') % 60
    return `${dd > 0 ? dd + ' d ': ''}${("0" + hr).slice(-2)}:${("0" + min).slice(-2)}:${("0" + sec).slice(-2)}`
}

const Timer = () => {
    const start = moment.unix(start_time);
    const [time, setTime] = useState(moment().unix())

    useEffect(() => {
        const timer = setInterval(() => {
            setTime(prevState => prevState + 1)
        }, 1000)

        return () => {
            clearInterval(timer)
        }
    }, [])

    return (
        <>
            <div>{start.format("HH:mm:ss")}</div>
            <div>{moment.unix(time).format("HH:mm:ss")}</div>
            <div>{restTime(start, time)}</div>
        </>
    )
}

export default Timer